jQuery.noConflict();
jQuery(document).ready(function($){



	$('html').addClass('js');


	var activeFontLoad = $.Callbacks();
	WebFont.load({
		google: {
			families: ['Open Sans:400,400i,700,700:latin-ext']
		},
		active: function() {
			activeFontLoad.fire();
		}
	});
	activeFontLoad.add(function() {
		$('.eshop-items .item>a, .eshop-items .item>a .content, .mod_custom-postage ul li, .mod_custom-filter .items .item, .mod_custom-products .items .item, .mod_custom-form-address .items .item, .basket_address .items .item').matchHeight({
			property: 'min-height',
		});
	});



	$('.mod_custom-filter .mod-title').on('click',function(){
		$(this).parents('.item').toggleClass('on');
	});




	$('.mod_custom-slideshow').each(function(){
		var i = $('.item',this).length;
		if(i>1){
			$(this).append('<div class="pager"/>');
			$('.items',this).cycle({
				fx: 'scrollHorz',
				slides: '.item',
				speed: 300,
				timeout: 10000,
				pauseOnHover: true,
				swipe: true,
				pager: $('.pager',this),
			});
		}
	});




	$('.detail-image').each(function(){
		var clone = $('ul',this).clone().addClass('slider');
		var i = $('li',this).length;
		$('ul',this).addClass('thumbnails').before(clone);
		if(i>1){
			$('.slider',this).prepend('<a class="prev"/><a class="next"/>');
			$('.slider',this).cycle({
				fx: 'scrollHorz',
				slides: 'li',
				speed: 300,
				timeout: 0,
				swipe: true,
				prev: $('.prev',this),
				next: $('.next',this),
				pager: $('.thumbnails',this),
				pagerTemplate: '',
			});
		}
	});



	$('.detail-panes').each(function(){
		var i = $('.pane',this).length;
		if(i>1){
			$(this).cycle({
				slides: '.pane',
				speed: 1,
				timeout: 0,
				pager: $('.tabs',this),
				pagerTemplate: '',
			});
		}
	});



	$('.slider_range').each(function(){
		$(this).wrapInner('<div class="slider"/>');
		$('input',this).hide();
		var data_min = $(this).data('min');
		var data_max = $(this).data('max');
		var data_step = $(this).data('step');
		var data_val = $(this).data('val');
		var input_from = $('[data-input="from"]',this).val();
		var input_to = $('[data-input="to"]',this).val();
		$('.slider',this).slider({
			range: true,
			min: data_min,
			max: data_max,
			step: data_step,
			values: [input_from,input_to],
			slide: function(event,ui) {
				$('.ui-slider-handle:first span',this).text(ui.values[0]+' '+data_val);
				$('.ui-slider-handle:last span',this).text(ui.values[1]+' '+data_val);
				$('[data-input="from"]',this).val(ui.values[0]);
				$('[data-input="to"]',this).val(ui.values[1]);
			}
		});
		$('.ui-slider-handle',this).prepend('<span/>');
		$('.ui-slider-handle:first span',this).text($('.slider',this).slider('values',0)+' '+data_val);
		$('.ui-slider-handle:last span',this).text($('.slider',this).slider('values',1)+' '+data_val);
	});







	// file input style
	$('input[type=file]').each(function(){
		var uploadText = $(this).data('value');
		if (uploadText) {
			var uploadText = uploadText;
		} else {
			var uploadText = 'Choose file';
		}
		var inputClass=$(this).attr('class');
		$(this).wrap('<span class="fileinputs"></span>');
		$(this)
			.parents('.fileinputs')
			.append($('<span class="fakefile"/>')
			.append($('<input class="input-file-text" type="text" />')
			.attr({'id':$(this)
			.attr('id')+'__fake','class':$(this).attr('class')+' input-file-text'}))
			.append('<input type="button" value="'+uploadText+'">'));
		$(this)
			.addClass('type-file')
			.css('opacity',0);
		$(this)
			.bind('change mouseout',function(){$('#'+$(this).attr('id')+'__fake')
			.val($(this).val().replace(/^.+\\([^\\]*)$/,'$1'))
		})
	});



	// placeholder
	if (document.createElement('input').placeholder==undefined){
		$('[placeholder]').focus(function(){
			var input=$(this);
			if(input.val()==input.attr('placeholder')){
				input.val('');
				input.removeClass('placeholder')
			}
		}).blur(function(){
			var input=$(this);
			if(input.val()==''||input.val()==input.attr('placeholder')){
				input.addClass('placeholder');
				input.val(input.attr('placeholder'))
			}
		}).blur()
	}



	// go top
	var go_top = function(){
		var offset = $(window).scrollTop();
		if(offset>100){
			$('#gotop').addClass('on');
		} else {
			$('#gotop').removeClass('on');
		}
	}
	go_top();
	$(window).scroll(function(){
		go_top();
	});
	$('#gotop a').click(function(){ 
		$('html,body').animate({scrollTop:0},600);
		return false;
	});



	// iCheck
	$('input[type="checkbox"],input[type="radio"]').iCheck();



	// selectBoxIt
	$('select:not([multiple])').selectBoxIt();


	// basictable
	$('table.normal').each(function(index, el) {
		// větší tabulka - povol zalamování
		if ($(this).find('th').length > 1)
		{
			$(this).basictable({
				tableWrapper: true
			});
		}
		// menší tabulka - nastav breakpoint na malý rozměr
		else
		{
			$(this).basictable({
				tableWrapper: true,
				breakpoint: 120 // css media query
			});
		}
	});



	/* funkce vykonané na připravenost dokumentu */
	onDocumentReady($);



	/* vyčká, než je dokončena jakakoliv událost */
	var waitForFinalEvent = (function () {
		var timers = {};
		return function (callback, ms, uniqueId) {
			if (!uniqueId) {
				uniqueId = "Don't call this twice without a uniqueId";
			}
			if (timers[uniqueId]) {
				clearTimeout (timers[uniqueId]);
			}
			timers[uniqueId] = setTimeout(callback, ms);
		};
	})();



	/* na opravdovou zmenu velikosti okna - konej */
	window._width = $(window).width();
	$(window).resize(function () {
		waitForFinalEvent(function(){

			// detekce skutečné změny
			if (window._width != $(window).width())
			{
				window._width = $(window).width();
				onWindowResize($);
			}

		}, 500, "afterWindowResize");
	});

});


/**
 * funkce vykonané na připravenost dokumentu
 *
 * @param   {function}  jQ  objekt jQuery
 * 
 * @return
 */
function onDocumentReady(jQ)
{
	uniteHeights(jQ);				// vyrovnej výšky elementů
	return;
}


/**
 * funkce vykonané na responzivní chování
 *
 * @param   {function}  jQ 	objekt jQuery
 *
 * @return
 */
function onWindowResize(jQ)
{
	uniteHeights(jQ);				// vyrovnej výšky elementů
	return;
}


/**
 * Funkce pro sjednocování elementů stránky
 * - na základě třídy (unite-heights)
 * a nastavené ID skupiny elementů (data-ug)
 *
 * @param   {function}  jQ 	objekt jQuery
 *
 * @return
 */
function uniteHeights(jQ)
{
	// seznam skupin
	var u_gs = [];
	jQ('.unite-heights').each(function(i, el) {
		var c_ug = jQ(this).attr('data-ug');
		if (jQ.inArray( c_ug, u_gs ) == -1)
		{
			u_gs.push(c_ug);
		}
	});

	// projdi všechny skupiny a nastav jim stejné výšky
	if (u_gs.length > 0)
	{
		jQ.each(u_gs, function(i, g_id) {

			var heighest = 0;
			s = '[data-ug="' + g_id + '"]';	// data unite group

			// zresetuj a načti nejvyšší
			jQ(s).each(function(index, el) {
				jQ(this).css('height', 'auto');	// reset
				heighest = jQ(this).height() > heighest ? jQ(this).height() : heighest;
			});

			// nastav nejvyšší
			jQ(s).each(function(index, el) {
				jQ(this).height(heighest);
			});
		});
	}
	return;
}